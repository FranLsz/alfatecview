﻿
$(document)
    .ready(function() {

        var $mxbtn1 = $("#mxbtn1");
        var $mxbtn2 = $("#mxbtn2");
        var $mxvid1 = $("#mx1vid");
        var $mxvid2 = $("#mx2vid");

        $mxvid1.show();

        $mxbtn1.on('click',
            function() {
                $mxvid2.hide();
                $mxvid1.show();
            });

        $mxbtn2.on('click',
            function() {
                $mxvid1.hide();
                $mxvid2.show();
            });

    });

